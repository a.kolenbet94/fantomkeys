const express = require('express');
const Fantom = require('./fantom.js');
const bodyParser = require('body-parser');
const web3 = require('web3');
const app = express();
const SimpleStorage = require('./build/contracts/SimpleStorage.json');

const port = 3001;
const fantom = new Fantom('http://18.221.128.6:8080');

app.use(bodyParser.json());
var blockNumber = 436;
const methods = {};
const transaction = ["0x3d67cca28c7e215c77390cb52028300ffc28517fde3b8bf0540bc6babe86e71b"];
methods['eth_getBlockByNumber'] = (req) => {
    return {
        "result": {
            "number": web3.utils.toHex(blockNumber), // 436
            "hash": "0xe670ec64341771606e55d6b4ca35a1a6b75ee3d5145a99d05921026d1527331",
            "parentHash": "0x9646252be9520f6e71339a8df9c55e4d7619deeb018d2a3f2d21fc165dde5eb5",
            "nonce": "0xe04d296d2460cfb8472af2c5fd05b5a214109c25688d3704aed5484f9a7792f2",
            "sha3Uncles": "0x1dcc4de8dec75d7aab85b567b6ccd41ad312451b948a7413f0a142fd40d49347",
            "logsBloom": "0xe670ec64341771606e55d6b4ca35a1a6b75ee3d5145a99d05921026d1527331",
            "transactionsRoot": "0x56e81f171bcc55a6ff8345e692c0f86e5b48e01b996cadc001622fb5e363b421",
            "stateRoot": "0xd5855eb08b3387c0af375e9cdb6acfc05eb8f519e419b874b6ff2ffda7ed1dff",
            "miner": "0x4e65fda2159562a496f9f3522f89122a3088497a",
            "difficulty": "0x027f07", // 163591
            "totalDifficulty": "0x027f07", // 163591
            "extraData": "0x0000000000000000000000000000000000000000000000000000000000000000",
            "size": "0x027f07", // 163591
            "gasLimit": "0x9f759", // 653145
            "gasUsed": "0x9f759", // 653145
            "timestamp": "0x54e34e8e", // 1424182926
            "transactions": transaction,
            "uncles": []
        }
    }
};

methods['eth_getTransactionByHash'] = (req) => {
    return {
        "result" : null
    }
};

methods['eth_getTransactionReceipt'] = (req) => {
    const txHash = req[0];

    return new Promise((resolve, reject) => {   
        try {
            setTimeout(() => {
                fantom.getTransaction(txHash, (result) => {
                    resolve({
                        "result": result
                    });
                }, 1000);
            });
        } catch (e) {
            reject(e);
        }
    });
};

methods[''] = (req) => {

}

methods['net_version'] = (req) => {
    return {
        "result": "1"
    }
};

methods['eth_getBalance'] = (req) => {
    const acc = req[0];
    return new Promise((resolve, reject) => {
        try {
            fantom.getAccount(acc, (result) => {
                resolve({
                    "result": web3.utils.toHex(result.balance)
                });
            })
        } catch (e) {
            reject(e);
        }
    });
};

methods['eth_getTransactionCount'] = (req) => {
    const acc = req[0];
    return new Promise((resolve, reject) => {
        try {
            fantom.getAccount(acc, (result) => {
                resolve({
                    "result": web3.utils.toHex(result.nonce)
                });
            })
        } catch (e) {
            reject(e);
        }
    });
}

methods['eth_sendRawTransaction'] = (req) => {
    blockNumber++;
    const tx = req[0];
    return new Promise((resolve, reject) => {   
        try {
            fantom.sendRawTransaction(tx, (result) => {
                console.log("Tx Send: " + result);
                transaction.push(result.txHash);
                resolve({
                    "result": result.txHash
                });
            })
        } catch (e) {
            reject(e);
        }
    });
};

methods['eth_call'] = (req) => {
    blockNumber++;
    const tx = req[0];
    return new Promise((resolve, reject) => {
        try {
            fantom.call(tx, (result) => {
                resolve({
                    "result": "0x0"
                });
            })
        } catch (e) {
            reject(e);
        }
    });
}

methods['eth_blockNumber'] = (req) => {
    blockNumber++;
    return {
        "result": web3.utils.toHex(blockNumber)
    }
}

methods['eth_getCode'] = (req) => {
    return {
        "result": SimpleStorage.bytecode
    };
}

methods['eth_estimateGas'] = (req) => {
    return {
        "result": "0x52080"
    };
}


app.all('*', (req, res) => {
    const body = req.body;

    console.log('Method: ' + body.method);
    console.log('Params: ' + JSON.stringify(body.params));

    const reply = methods[body.method](body.params);
    if (typeof reply.then === 'function') {
        reply.then(result => {
            result.id = body.id;
            result.jsonrpc = body.jsonrpc;

            res.send(result);
        });
    } else {
        reply.id = body.id;
        reply.jsonrpc = body.jsonrpc;

        res.send(reply);
    }
});




app.listen(port, function () {
    console.log(`Fantom JSON RPC started on ${port}!`);
});