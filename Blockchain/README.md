# BlockScribe
> Decentralized subscription marketplace on Fantom blockchain


One to two paragraph statement about your product and what it does.

## Usage example

User set up here

## Development setup

Describe how to install all development dependencies and how to run an automated test-suite of some kind. Potentially do this for multiple platforms.

```sh
npm install 
cd client
npm install
```

## Development setup

```sh
# Deploy contract
npm run deploy
# Run fantom proxy
npm run proxy
# Run frontend application
cd client
npm run start
```

## Release History

## Meta

## Contributing
