const axios = require('axios');
const EthereumTx = require('ethereumjs-tx');
const Web3 = require('web3');
const fs = require('fs');
const Fantom = require('./fantom.js');

const web3 = new Web3(new Web3.providers.HttpProvider('http://18.221.128.6:8080'));
const fantom = new Fantom('http://18.221.128.6:8080');

const contractName = 'LockingContract';

const account = {
    address: '0xd3bee5ae53dfbc3a81e62ae2cba2cfe75cace6b5',
    key: Buffer.from('87B69733C23BD3B8FEADA5B656DD9C5A6195632385EA05B09891BA7F3CBF8057', 'hex')
};

function generateRawTransaction(txParams, cb) {
    fantom.getAccount(txParams.from, acc => {
        txParams.nonce = web3.utils.toHex(acc.nonce);

        const tx = new EthereumTx(txParams);
                
        tx.sign(account.key);

        const serializedTx = tx.serialize();
        const txHex = '0x' + serializedTx.toString('hex');
        cb(txHex);
    });
}

const contractCompiled = JSON.parse(fs.readFileSync(`build/contracts/${contractName}.json`, 'utf8'));
const abi = contractCompiled.abi;
const bytecode = contractCompiled.bytecode;
const contract = new web3.eth.Contract(abi);
console.log('BC:' + bytecode);
const contractData = contract.deploy({'data': bytecode}).encodeABI();

const txParams = {
    gasPrice: '0x000000000001',
    gasLimit: '0x271000',
    from: account.address,
    value: '0x00',
    data: contractData,
    chainId: 1
};

generateRawTransaction(txParams, txHex => {
    console.log('Created transaction: ' + txHex);

    fantom.sendRawTransaction(txHex, result => {
        console.log(result);

        const txHash = result.txHash;
        fantom.getTransaction(txHash, tx => {
            console.log("Deployed contract at: "+ tx.contractAddress);
        });
    });
});