const axios = require('axios');

/**
 * Wrapper around Fantom REST API
 */
class Fantom {

    constructor(url) {
        this.url = url;
    }

    /**
     * Sends raw transaction to blockchain. Note that transaction should be signed prior to sending.
     * 
     * @param {String} txHex transaction hex string
     * @param {cb} cb callback function
     */
    sendRawTransaction(txHex, cb) {
        axios.post(this.url + '/sendRawTransaction', txHex).then(function (response) {
            cb(response.data);
        }).catch(function (error) {
            console.log(error);
        });
    }

    /**
     * Returns account details such as balance (in wei) and nonce (transaction count)
     * 
     * @param {String} account account (public key) address
     * @param {cb} cb callback function
     */
    getAccount(account, cb) {
        axios.get(this.url + '/account/' + account).then(function (response) {
            cb(response.data);
        }).catch(function (error) {
            console.log(error);
        });
    }

    call(callData, cb) {
        axios.post(this.url + '/call', callData).then(function (response) {
            cb(response.data);
        }).catch(function (error) {
            console.log(error);
        });
    }

    getTransaction(txHash, cb) {
        setTimeout(() => {
            axios.get(this.url + '/transaction/' + txHash).then(function (response) {
                cb(response.data);
            }).catch(function (error) {
                console.log(error);
            });
        }, 2000);
    }
}

module.exports = Fantom;