pragma solidity ^0.4.24;

import "./ECDSA.sol";

contract LockingContract {

    struct Lock {
        uint lockId;
        uint dateFrom;
        uint dateTo;
        address owner;
    }

    mapping (address => Lock[]) addressLock;
    mapping (uint => Lock[]) allLock;

    function book(uint _lockId, uint _dateFrom, uint _dateTo, address _owner) 
    external
    payable
    {
        require(_lockId != 0);
        require(_dateFrom != 0);
        require(_dateTo != 0);
        require(_dateFrom <= _dateTo);
        
        require(!isBooked(_lockId, _dateFrom, _dateTo));
        
        address owner;
        if (_owner == address(0)) {
            owner = msg.sender;
        } else {
            owner = _owner;
        }

        Lock memory lock = Lock(_lockId, _dateFrom, _dateTo, owner);
        addressLock[msg.sender].push(lock);
        allLock[_lockId].push(lock);
    }
    
    function isBooked(uint _lockId, uint _dateFrom, uint _dateTo) public view returns (bool) {
        Lock[] memory locks = allLock[_lockId];
        for (uint i = 0; i < locks.length; i++) {
            Lock memory lock = locks[i];
            
            if (_dateFrom >= lock.dateFrom && _dateFrom <= lock.dateTo) {
                return true;
            }
            
            if (_dateTo >= lock.dateFrom && _dateFrom <= lock.dateTo) {
                return true;
            }
        }
        
        return false;
    }

    function signedInfo(uint _lockId, uint _date) public pure returns (bytes32) {
        bytes memory data = abi.encodePacked(_lockId, _date);
        return keccak256(data);
    }
    
    function recover(bytes32 _hash, bytes _signature) public pure returns (address) {
        return ECDSA.recover(_hash, _signature);
    }
    
    function getAddress(uint _lockId, uint _date, bytes _signature) public pure returns (address) {
        bytes32 hash = signedInfo(_lockId, _date);
        address addr = recover(hash, _signature);
        
        return (addr);
    }
    
    function checkLock(uint _lockId, uint _date, bytes _signature) public view returns (bool) {
        address addr = getAddress(_lockId, _date, _signature);
        
        Lock[] memory addressLocks = addressLock[addr];
        for (uint i = 0; i < addressLocks.length; i++) {
            Lock memory lock = addressLocks[i];

            if (lock.owner == addr
                && lock.dateFrom <= _date
                && lock.dateTo >= _date
                && lock.lockId == _lockId) {
                    return (true);
                }

        }
        return (false);
    }
}