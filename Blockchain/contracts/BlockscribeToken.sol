pragma solidity ^0.4.24;

import "openzeppelin-solidity/contracts/token/ERC20/MintableToken.sol";

/**
 * @title BlockscribeToken
 * @dev Very simple ERC20 Token that can be minted.
 * It's meant to be used as stable token to pay subscriptions in. 
 * Paying subscriptions in native token is undesirable because of price volatility.
 */
contract BlockscribeToken is MintableToken {

  string public constant name = "BlockScribe Token";
  string public constant symbol = "BST";
  uint8 public constant decimals = 18;

}