module.exports = {
  networks: {
    development: {
      host: "18.221.128.6",
      port: 8080,
      network_id: "*" // Match any network id
    }
  }
};