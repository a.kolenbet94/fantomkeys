import React, {
  Component
} from "react";
import SimpleStorageContract from "./contracts/SimpleStorage.json";
import getWeb3 from "./utils/getWeb3";
import Fantom from "./utils/fantom";
import EthereumTx from "ethereumjs-tx";

import "./App.css";

const addr = "0x7854ab809fcf4940d6f317af558ae39e516e31a4";

class App extends Component {
  state = {
    storageValue: 0,
    web3: null,
    accounts: null,
    contract: null,
    fantom: null
  };

  componentDidMount = async () => {
    try {
      const fantom = new Fantom('http://18.221.128.6:8080');
      // Get network provider and web3 instance.
      const web3 = await getWeb3();

      // Use web3 to get the user's accounts.
      const accounts = await web3.eth.getAccounts();

      const instance = new web3.eth.Contract(SimpleStorageContract.abi, addr);

      // Set web3, accounts, and contract to the state, and then proceed with an
      // example of interacting with the contract's methods.
      this.setState({
        web3,
        accounts,
        contract: instance,
        fantom: fantom
      }, this.runExample);
    } catch (error) {
      // Catch any errors for any of the above operations.
      alert(
        `Failed to load web3, accounts, or contract. Check console for details.`
      );
      console.log(error);
    }
  };

  increment = async () => {
    const {
      storageValue,
      accounts,
      contract,
      fantom,
      web3
    } = this.state;

    const self = this;

    const callData = contract.methods.set(storageValue + 1).encodeABI();
    console.log(callData);




    const txParams = {
      from: accounts[0],
      to: contract.options.address,
      value: '0x00',
      gasLimit: '0x9ddd4',
      gasPrice: '0x00000000001',
      data: callData,
      chainId: 1
    };

    fantom.getAccount(txParams.from, acc => {
      txParams.nonce = web3.utils.toHex(acc.nonce);


      console.log('Before signing!');
      if (false) {
        self.signHardcode(fantom, txParams);
      } else {
        self.signMetamask(web3, fantom, txParams, self);
      }
    });
  };

  signHardcode = (fantom, txParams) => {
    const pk = "87B69733C23BD3B8FEADA5B656DD9C5A6195632385EA05B09891BA7F3CBF8057";
    const tx = new EthereumTx(txParams);

    tx.sign(Buffer.from(pk, 'hex'));

    const serializedTx = tx.serialize();
    const txHex = '0x' + serializedTx.toString('hex');

    console.log(txHex);

    fantom.sendRawTransaction(txHex, result => {
      console.log(result);
    });
  }

  signMetamask = (web3, fantom, txParams, self) => {
    web3.eth.sendTransaction(txParams, (error, result) => {

      fantom.getTransaction(result, () => {
        self.runExample();
      });
    });
  }

  sign = () => {
    const { web3, accounts } = this.state;

    const result = web3.utils.soliditySha3({t: 'uint', v: 1}, {t: 'uint', v: 1539807565889});
    web3.eth.sign(result, accounts[0]).then(console.log);
  }

  runExample = async () => {
    const {
      accounts,
      contract,
      fantom,
      web3
    } = this.state;

    const callData = contract.methods.get().encodeABI();

    console.log(callData);

    const tx = {
      from: accounts[0],
      value: 0,
      to: contract.options.address,
      data: callData,
      nonce: 0,
      gas: 1000000,
      gasPrice: 0
    };

    const app = this;

    fantom.call(JSON.stringify(tx), result => {
      console.log(result);
      app.setState({
        storageValue: web3.utils.hexToNumber(result.data)
      });
    });
  };

  render() {
    if (!this.state.web3) {
      return <div> Loading Web3, accounts, and contract... </div>;
    }

    const {
      accounts,
      contract
    } = this.state;
    return ( <div className = "App" >
      <h1> Simple counter on Fantom DAG </h1>
      <p> Current user: {
        accounts[0]
      } </p>
      <h2>  Address: {
        contract.options.address
      } </h2> <p>
      Our contract is deployed.Here we can interact with it 
      </p> <div> The stored value is: {
        this.state.storageValue
      } < button onClick = {
        this.increment
      } > + </button></div>
      <button onClick = {this.sign}>Sign</button>
      </div>

      
    );
  }
}

export default App;