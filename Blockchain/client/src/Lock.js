import React, {
  Component
} from "react";
import LockContract from "./contracts/LockingContract.json";
import getWeb3 from "./utils/getWeb3";
import Fantom from "./utils/fantom";
import EthereumTx from "ethereumjs-tx";

import "./App.css";
import { decode } from "punycode";

const addr = "0xd0c040a4b1452fa80044c8c9f173a2b356f1cb4c";

class App extends Component {
  state = {
    storageValue: 0,
    web3: null,
    accounts: null,
    contract: null,
    fantom: null,
    book: {}
  };

  componentDidMount = async () => {
    const dateToStr = d => `${d.getFullYear()}-${d.getMonth() + 1}-${d.getDate()}`;

    try {
      const fantom = new Fantom('http://18.221.128.6:8080');
      // Get network provider and web3 instance.
      const web3 = await getWeb3();

      // Use web3 to get the user's accounts.
      const accounts = await web3.eth.getAccounts();

      const instance = new web3.eth.Contract(LockContract.abi, addr);

      // Set web3, accounts, and contract to the state, and then proceed with an
      // example of interacting with the contract's methods.
      this.setState({
        web3,
        accounts,
        contract: instance,
        fantom: fantom,
        book: {
          room: "2",
          from: dateToStr(new Date()),
          to: dateToStr(new Date())
        }
      }, this.runExample);
    } catch (error) {
      // Catch any errors for any of the above operations.
      alert(
        `Failed to load web3, accounts, or contract. Check console for details.`
      );
      console.log(error);
    }
  };

  increment = async () => {
    const {
      storageValue,
      accounts,
      contract,
      fantom,
      web3,
      book
    } = this.state;

    const self = this;
    const room = book.room;
    const from = new Date(book.from).getTime();
    const to = new Date(book.to).getTime();

    console.log('Room: ' + room);
    console.log('From: ' + from);
    console.log('To: ' + to);
    console.log('Account: ' + accounts[0]);
    const callData = contract.methods.book(room, from, to, accounts[0]).encodeABI();

    console.log(callData);

    const txParams = {
      from: accounts[0],
      to: contract.options.address,
      value: "0x00",
      gasLimit: '0x9ddd4',
      gasPrice: '0x00000000001',
      data: callData,
      chainId: 1
    };

    fantom.getAccount(txParams.from, acc => {
      txParams.nonce = web3.utils.toHex(acc.nonce);


      console.log('Before signing!');
      self.signMetamask(web3, fantom, txParams, self);
    });
  };

  signMetamask = (web3, fantom, txParams, self) => {
    web3.eth.sendTransaction(txParams, (error, result) => {
      console.log(result);
      fantom.getTransaction(result, () => {
        self.runExample();
      });
    });
  }

  sign = () => {
    const { web3, accounts } = this.state;

    const result = web3.utils.soliditySha3({t: 'uint', v: 1}, {t: 'uint', v: 1539807565889});
    web3.eth.sign(result, accounts[0]).then(console.log);
  }

  runExample = async () => {
    const {
      accounts,
      contract,
      fantom,
      web3,
      book
    } = this.state;

    const thisState = this.state;

    const room = book.room;
    const from = new Date(book.from).getTime();
    const to = new Date(book.to).getTime();

    const callData = contract.methods.isBooked(room, from, to).encodeABI();
    const result = web3.eth.abi.encodeParameters(['uint', 'uint', 'uint'], [room, from, to]);

    console.log(result);
    console.log(callData);

    const tx = {
      from: accounts[0],
      value: 0,
      to: contract.options.address,
      data: callData,
      nonce: 0,
      gas: 1000000,
      gasPrice: 0
    };

    const app = this;

    fantom.call(JSON.stringify(tx), result => {
      const decoded = 0 !== web3.utils.hexToNumber(result.data);
      console.log('Result: ' + result.data + ' decoded to ' + decoded);

      const upd = { book: { ...thisState.book,  state: decoded } };

      app.setState(upd);
    });
  };

  render() {
    if (!this.state.web3) {
      return <div> Loading Web3, accounts, and contract... </div>;
    }

    const {
      accounts,
      contract
    } = this.state;

    return ( 
      <div className = "App" >
        <h1> Simple rental service on Fantom. </h1> 
        <h2> Pay FTM to reserve a room. <br/> Unlock with mobile phone.</h2>
        <p> <b>Account: </b> { accounts[0] } </p>
        <p> <b>Contract: </b> { contract.options.address} </p> 
        <p> Select a room and book it with a form below </p> 

        <div>
          <p>Room state is: {this.state.book.state === '?' ? '?' :  this.state.book.state === false ? 'free' : 'booked'}</p>
        </div>
        <div>
          <select
            onChange = { e => { 
              const upd = { book: { ...this.state.book,  room: e.target.value } };
              this.setState(upd, this.runExample);
            }}
            value = { this.state.book.room }>
            <option value="1">Room 1</option>
            <option value="2">Room 2</option>
            <option value="3">Room 3</option>
            <option value="4">Room 4</option>
            <option value="5">Room 5</option>
            <option value="6">Room 6</option>
          </select>
          <input type="date"
            onChange = { e => { 
              const upd = { book: { ...this.state.book,  from: e.target.value } };
              this.setState(upd, this.runExample);
            }}
            value = { this.state.book.from }/>
          <input type="date"
            onChange = { e => { 
              const upd = { book: { ...this.state.book,  to: e.target.value } };
              this.setState(upd, this.runExample);
            }}
            value = { this.state.book.to }/>
          <button onClick={this.increment}>Book</button>
        </div>
        <button onClick = {this.sign}>Test Sign</button>
      </div>

      
    );
  }
}

export default App;