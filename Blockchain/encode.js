const EthereumTx = require('ethereumjs-tx');
const Web3 = require('web3');
const Fantom = require('./fantom.js');

const web3 = new Web3(new Web3.providers.HttpProvider('http://18.221.128.6:8080'));
const fantom = new Fantom('http://18.221.128.6:8080');

const types = ['string', 'uint256'];
const data = ["123", 1539528523678];

const result = web3.utils.soliditySha3({t: 'string', v: '123'}, {t: 'uint256', v: 1539528523678});
const result2 = web3.utils.sha3(result);

const signature = "0xe5d398fec808d22324e1c185baaab50bd8459ea7b1b20f5705521a36d620e3f97981d76a5e8229b5baac5d126369e29715c630e8759cb5a491ab579e3d6e589401";

console.log(result);
console.log(result2);

// data:        0xc8210b28bcb653dc6cab81730b04fca412bd341d5abd173553109b75ab92c4a5
// dataHash:    0x913064e42c4ce3effeaf0c983040458af830d5a2a9e17d9b1a1b1d381a290978
// signature:   0x94150b3d6f4d84b8f7c2ee23050dd7cdda91fd80e5a7aa40e5853a413d8c4f9536641e52ba612b40fb8fed5eea9bd366249365d4f57da83bb87e4a8d41a5a5311b