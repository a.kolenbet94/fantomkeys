# FantomKeys
Solution architecture combines power of Blockchain and IoT to create secure Android application allowing user to open blockchain-enabled smart locks with their phone. Application is based on Fantom blockchain which provides extreme scalability and transaction speed with DAG technology. After paying with Fantom Coin a record is created in distributed ledger, that an owner of a given public/private key pair may open given lock at current time. Then user can touch the smart lock with their phone to unlock it. Smart lock will communicate with phone via NFC by sending it's identifier and a current timestamp. Android app will sign given request with owners's private key proving the authority of a phone holder. Then the signed request is redirected by the lock to blockchain. Smart contract will then verify the signature and request parameters. If everything is OK, it will send lock a command to unlock.


## Run a first Docker Image
To read the container registry images, you'll need to install Docker.

Then run command:

docker load --input proxy.tar

docker images

docker run -i -t -p 3001:3001 [image id]

## Run a second Docker Image
Run command:

docker load --input client.tar

docker images

docker run -i -t -p 3000:3000 [image id]

## Getting Started

### Scenario 1:
In browser open http://localhost:3000

Run Metamask user with a positive balance test Fantom Coins 

Use TestUser with Wallet Seed: "fabric seven expect slab hurt lyrics language exercise hungry have deliver tray"

In start page check Room and date.

See that Room status is "free"

Then click button "Book"

Sign transaction with Metamask

See that Room status changed to "booked"

### Scenario 2:
Launch on Phone Android app "Fantom Keys".

Select the current date and click the button "Reserv".

Run sketch FantomEsp32.ino for IoT Smart Door lock system.

Activate on the NFC Phone and bring to the IoT Smart door lock.

When check is successful system transmit a signal to unlock.

## Contact
Team member: Vasin Denis

mailto: akafakir@gmail.com
